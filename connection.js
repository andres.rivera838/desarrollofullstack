const debug = require('debug')('popupart:api:db')
const client = require('mongoose')

const urlMongo = 'mongodb://popupart:popupart123@ds131323.mlab.com:31323/popupart'

var db = client.connect(urlMongo, { useNewUrlParser: true }, (err, success) => {
  if (err) {
    debug(`Error: ${err.message}`)
  } else {
    debug('Connecting to dataBase')

    return success
  }
})

module.exports = {
  db,
  auth: {
    secret: process.env.SECRET || 'admin'
  }
}