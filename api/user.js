'use strict'

const debug = require('debug')('fullStack:user:routes')
const express = require('express')
const asyncify = require('express-asyncify')
const chalk = require('chalk')

const config = require('../connection')

const user = asyncify(express.Router())
const User = require('../schema/schemaUser')

const bodyParser = require('body-parser')

user.use(bodyParser.urlencoded({ 'extended': true }))
user.use(bodyParser.json({ type: '*/*' }))

let services

user.use('*', async (req, res, next) => {
  if (!services) {
    debug('Connecting to services')
    try {
      services = await config.db
    } catch (e) {
      return next(e)
    }
  }
  next()
})


user.post('/register', async (req, res, next) => {
  console.log(`${chalk.green('[fullStack - api/user]')} /register`)
  const { name, lastName, userName, password, currency } = req.body

  let newUser = new User({ name, lastName, userName, password, currency, date: new Date() })

  User.collection.insert([newUser])
    .then(async (data) => {
      console.log("data", data)
      response(res, data, 200)
    }).catch((err) => {
      console.error(`${chalk.red('[fatal error]')} ${err}`)
      response(res, err, 500)
    })
})

function response(res, data, status) {
  res.status(status)
  res.send({ status: status, data: data })
}

module.exports = user
