const mongoose = require('mongoose')

const userDevSchema = mongoose.Schema({
  name: String,
  lastName: String,
  userName: String,
  password: String,
  currency: String,
  date: Date
})
module.exports = mongoose.model('userDev', userDevSchema)
