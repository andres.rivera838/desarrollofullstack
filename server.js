'use strict'

const debug = require('debug')('fullStack:api')
const http = require('http')
const chalk = require('chalk')
const express = require('express')
const asyncify = require('express-asyncify')
const cors = require('cors')

const user = require('./api/user')

const port = process.env.PORT || 3000
const app = asyncify(express())
const server = http.createServer(app)

const bodyParser = require('body-parser')

var publicDir = require('path').join(__dirname, '../public')
app.use(express.static(publicDir))

// Tell the bodyparser middleware to accept more data
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

app.use(cors())

app.use('/user', user)

// Express Error Handler
app.use((err, req, res, next) => {
  debug(`Error: ${err.message}`)

  if (err.message.match(/not found/)) {
    return res.status(404).send({ error: err.message })
  }

  res.status(500).send({ error: err.message })
})

function handleFatalError (err) {
  console.error(`${chalk.red('[fatal error]')} ${err.message}`)
  console.error(err.stack)
  process.exit(1)
}

// si no es un modulo padre lanza el servidor de lo contrario se exporta. en el momento en que se requiere el server
if (!module.parent) {
  process.on('uncaughtException', handleFatalError)
  process.on('unhandledRejection', handleFatalError)

  server.listen(port, () => {
    console.log(`${chalk.green('[FullStack]')} server listening on port ${port}`)
  })
}

module.exports = server
